//-----------------
// Laboration 5, Assignment_1A.cpp
// Program som arbetar med en c arrayer
// Marcus Lönnqvist 2018-01-27
//-----------------

#include <iostream>
#include <iomanip>
#include <ctime>
#include <random>

using namespace std;
int maxNum(int* array, size_t size);
int minNum(int* array, size_t size);
void printArray(int* array, size_t size);
void sortArray(int* array, size_t size);
int mostCommon(int* array, size_t size);
int foundNumber(int* array, size_t size, int input);

int main()
{
 random_device rd;
 default_random_engine gengerator(rd());
 uniform_int_distribution<int>distribution(1,100);
 //Ser till så att jag kan få fram ett slumvalt nummer mellan 1 - 100

 srand(time(0));

 const size_t size = 600;
 int amountOf = 0, input, randomNumbers[size], sortedArray[size], sum = 0;
 float avarage;

 cout << "-----Laboration 5 Assignment 1A-----" << endl << endl;

 for(size_t i = 0; i < size; i++)
 {
   randomNumbers[i] = distribution(gengerator);
 }
 //Slumpar talen i arrayen

 for(size_t i = 0; i < size; i++)
 {
   sortedArray[i] = randomNumbers[i];
 }
 //Kopierar Arrayen

 for (size_t i = 0; i < size; i++)
 {
   sum += sortedArray[i];
 }
 avarage = sum/size;
 //Räknar ut summan och medelvärdet av arrayen
 sortArray(sortedArray, size);
 //Sorterar Arrayen

 printArray(sortedArray, size);
 //Skriver ut den sorterade arrayen
 cout << endl << "^----------Sorted Array----------^";
 cout << endl << endl << "Average: " << avarage << endl << endl;
 cout << "The most frequent number is: " << mostCommon(sortedArray, size) << endl;
 cout << "The biggest number is: " << maxNum(sortedArray, size) << endl;
 cout << "The smallest number is: " << minNum(sortedArray, size) << endl;

 cout << endl << "Enter a number to search for in the Array: ";
 cin >> input;
 for(size_t i = 0; i < size; i++)
 {
   if (sortedArray[i] == input)
   {
     amountOf++;
   }
 }
 //Låter användaren söka i arrayen

 cout << endl << endl << "The number " << input << " was found " << amountOf << " times in the Array";


  return 0;
}

int mostCommon(int* array, size_t size)
{
  int tempMax = 0, temp, compare, max;
  for(size_t i=0; i < size; i++)
  {
    temp = 0;
    compare = array[i];
    for(int j = 0; j < size; j++)
    {
      if(array[j] == compare) //Om denna arrayplatsen = nästa arrayplats
      {
        temp++;
      }
    }
    if (temp > tempMax) //Om temp < förra maxantalet så blir tempmax det nya maxantalet
    {
      max = compare;
      tempMax = temp;
    }

  }
  return max;

}
void printArray(int* array, size_t size)
{
  cout << endl << endl << "Sorted Array: " << endl << endl;
  for (size_t i = 0; i < size; i++)
  {
    cout << left << setw(4) << array[i];
    if(array[i]< array[i+1])
    cout << endl;
  }

  //Skriver ut Arrayen
  return;
}

void sortArray(int* array, size_t size)
{

  for(int i=0; i < size - 1; i++)
  {
    for(size_t j=0; j < size - i; j++)
    {
      if(array[j] > array[j+1])
      {
        int temp = array[j];
        array[j] = array[j+1];
        array[j+1] = temp;
      }
    }
  }
  return;
  //Sorterar Arrayen med Bubble-sort algoritmen
}
int maxNum(int* array, size_t size)
{
  int max = 0;
  for(size_t i = 0; i < size; i++)
  {
    if(array[i] > max)
    {
      max = array[i];
    }
  }
  return max;
}
int minNum(int* array, size_t size)
{
  int min = 100;
  for(size_t i = 0; i < size; i++)
  {
    if(array[i] < min)
    {
      min = array[i];
    }
  }
  return min;
}

//-----------------
// Laboration 5, Assignment_2.cpp
// Program som arbetar med en vektor
// Marcus Lönnqvist 2019-01-30
//-----------------

#include <iostream>
#include <iomanip>
#include <ctime>
#include <random>

using namespace std;

void binarySearch(int* array, int size, int in);
void sortArray(int* array, int size);
void printArray(int* array, int size);

int main()
{
 random_device rd;
 default_random_engine generator(rd());
 uniform_int_distribution<int>distribution(1,100);
 //Ser till så att jag kan få fram ett slumvalt nummer mellan 1 - 100

 srand(time(0));

 const int size = 100;
 int array[size];
 int input;

 cout << "-----Laboration 5 Assignment 2-----" << endl << endl;

 for(int i = 0; i <= size; i++)
 {
   array[i] = distribution(generator);
 }

 sortArray(array, size);
 printArray(array, size);
 //Sorterar för att kunna söka binärt

 cout << endl << endl << "Enter a number to search for: ";
 cin >> input;

 binarySearch(array, size, input);

 //Frågar och söker efter numbret användaren matat in
 return 0;
}
//Funktioner

void binarySearch(int* array, int size, int in)
{
 string wait;
 int tempNum;
 int count = 0;
 int max = size; //Storleken på arrayen, "max" blir så kallat taket i sökningen
 int min = 0; //Så kallat golvet i sökningen

 while(min <= max)
 {
   tempNum = floor((min + max)/2);
   //Temporär variabel delas med två så att den söker exakt mitt emällan min och max
   if(array[tempNum] < in)
   {
     min = tempNum + 1;
   }
   //Om sökt värde är större än värdet på denna arrayplats blir minimumvärdet
   //arrayplatsen adderat med 1
   else if(array[tempNum] > in)
   {
     max = tempNum - 1;
   }
   //Samma sak som ovan, skillnaden är att taket(maxvärdet) för sökningen blir mindre
   else
   {
     break;
   }
   //Skulle sökningen hitta värdet bryter den ur loopen

   count++; //Räknar varje steg sökningen gör


 }
 if(in == array[tempNum])
 {
  cout << endl << "found " << array[tempNum] << " in " << count <<" steps" <<  endl;
 }
 //Om sökt värde matchar arrayplatsens värde skriver programmet ut vilket värde som hittades
 else
 {
   cout <<endl << "The number you entered could not be found.";
 }

 return;
}
void sortArray(int* array, int size)
{

  for(int i=0; i < size - 1; i++)
  {
    for(int j=0; j < size - i; j++)
    {
      if(array[j] > array[j+1])
      {
        int temp = array[j];
        array[j] = array[j+1];
        array[j+1] = temp;
      }
    }
  }
  //Sorterar Arrayen med Bubble-sort algoritmen
  return;
}
void printArray(int* array, int size)
{

  cout << endl << endl << "Sorted Array: " << endl << endl;
  for(int i=0; i < size; i++)
  {
    cout << setw(4) << array[i];
    if((i+1)%20 == 0) // Ny rad för var 10:e element
    cout << endl;
  }
  //Skriver ut Arrayen

  return;
}

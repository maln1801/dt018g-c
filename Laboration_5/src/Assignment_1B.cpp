//-----------------
// Laboration 5, Assignment_1B.cpp
// Program som arbetar med en vektor
// Marcus Lönnqvist 2018-01-06
//-----------------

#include <iostream>
#include <iomanip>
#include <ctime>
#include <random>
#include <algorithm>

using namespace std;

void printSortedVector(vector<int> v);
int searchInVector(vector<int> v, int in);
int mostCommon(vector<int> v);

int main()
{
 random_device rd;
 default_random_engine generator(rd());
 uniform_int_distribution<int>distribution(1,100);
 //Ser till så att jag kan få fram ett slumvalt nummer mellan 1 - 100

 srand(time(0));

 const size_t size = 600;
 vector <int> randomNumbers;
 vector <int> copiedVector(size);
 int input;

 cout << "-----Laboration 5 Assignment 1B-----" << endl << endl;

 for(int i = 0; i < size; i++)
 {
   randomNumbers.push_back(distribution(generator));
 }
 //pushar alla random-nummer på plats

 copy(randomNumbers.begin(), randomNumbers.end(), copiedVector.begin());
 //Kopierar ursprungliga vektorn

 sort(copiedVector.begin(), copiedVector.end());
 //Sorterar Vektor

 printSortedVector(copiedVector);
 //Skriver ut vektor likt C-Array

 cout << endl << endl << "The smallest number is "  << *min_element(copiedVector.begin(), copiedVector.end()) << endl;
 cout << "The biggest number is " << *max_element(copiedVector.begin(), copiedVector.end()) << endl;
 cout << "The average number is " << (accumulate(copiedVector.begin(), copiedVector.end(), 0.0))/size << endl;
 cout << "The most common number is " << mostCommon(copiedVector) << endl;
 //Skriver ut beräknade värden

 cout << endl << endl << "Enter a number to search for: ";
 cin >> input;
 cout << endl << "Your number was found " << count(copiedVector.begin(), copiedVector.end(), input) << " times." << endl;
 //Frågar och söker efter numbret användaren matat in
 return 0;
}
//Funktioner
void printSortedVector(vector<int> v)
{
  for(int i = 0; i < v.size(); i++)
  {
    cout << left << setw(4) << v[i];
    if(v[i] < v[i+1])
    {
      cout << endl;
    }
  }

  return;
}
int mostCommon(vector<int> v)
{
  int tempMax = 0, temp, compare, max;
  for(int i=0; i < v.size(); i++)
  {
    temp = 0;
    compare = v[i];
    for(int j = 0; j < v.size(); j++)
    {
      if(v[j] == compare) //Om denna arrayplatsen = nästa arrayplats
      {
        temp++;
      }
    }
    if (temp > tempMax) //Om temp < förra maxantalet så blir tempmax det nya maxantalet
    {
      max = compare;
      tempMax = temp;
    }
  }
  return max;
}

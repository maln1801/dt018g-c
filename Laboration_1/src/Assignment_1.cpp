//-----------------
// Laboration 1, Assignment_1.cpp
// Program som räknar ut Milkostnad och bensinsförbrukning
// Marcus Lönnqvist 2018-11-06
//-----------------

#include <iostream>
#include <iomanip>
using namespace std;


int main()
{
  double ms1;
  double ms2;
  double l;
  double literPerMil;
  double milkostnad;
  const double blyfritt95 = 15.06; // kr per liter
 //Deklarerar varieblerna för varje indata och konstant

  cout << "----- Fuelconsumption and cost -----" << endl << endl;
  //Titel

  cout << "Enter mileage at the previous refuel [km]: ";
  cin >> ms1;
  cout << "Enter mileage at this refuel         [km]: ";
  cin >> ms2;
  cout << "How much did you put in the tank?     [l]: ";
  cin >> l;
  cout << endl << endl;
  //Hoppar ner två rader för snyggare utskrift
  literPerMil = (l/(ms2-ms1))*10;
  milkostnad = (blyfritt95*literPerMil);
  //Räknar ut milkostanden och liter per mil
  cout.precision(2);

  //Ställer in så nästa utskrift blir med 2 decimaler.
  cout << "Fuel consumption                  [l/mil]: " <<setw(10) << fixed << literPerMil << endl;
  cout << "Cost per mil                     [kr/mil]: " <<setw(10)  <<milkostnad;
  //Jag ändrade utskriften så att decimaltecknet skrivs ut på samma kolumn
  return 0;

}

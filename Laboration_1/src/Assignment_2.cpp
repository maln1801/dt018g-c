//-----------------
// Laboration 1, Assignment_2.cpp
// Program with both logical and syntactical errors
// Erik Ström 2018-11-02
//-----------------

using namespace std;
#include <iostream>
// Man måste ha med ett antal bibliotek för att kompilatorn ska förstå vad du vill köra.
// Som nu har du försökt köra "Cout" utan det biblioteket.
// Syntax-fel

int main() {
// Variables and constants
int radius, circumference, area;
const float PI = 3.14;
// Punkt ska skilja decimaler. Annars tar den talen som separata alltså 3 och 14.
// Syntax-fel

// Input the circles radius
cout << "Assign the circle's radius: ";
cin >> radius;
//cin behöver pilar ">>" istället för "="
// Syntax-fel

// Algorithm to calculate circumference (2*PI*r) and area (PI*r*r)
area = PI * radius * radius;
circumference = 2 * PI * radius;
// De räknar ut varandra. De är fel uträkningar i fel variabler.
// pi måste stå med versaler annars känner den inte igen variabeln
// Logiskt fel på uträkningen men syntacx-fel på hur du skrivit pi

// Output of results
cout << "A circle with the radius " << radius << " has the circumference " << circumference << " and area " << area << endl;
// Ovan var det ett radbyte i koden, då såg den inte "hartassarna" som avslutade
//Syntax-fel

// Validate x
cout << endl << "Enter a number to validate: ";
//La till ett "promt" att skriva in ett nummer för extra klarhet.
int x;
cin >> x;
if(x == 100) // Här var det bara ett "lika med" tecken. Då gör den x till 100
//Logiskt fel
cout << "x is equal to 100" << endl;
if(x > 0) //Det får inte vara ett ";" då nästa cout inte bryr sig om if satsen då.
//Logiskt fel
cout << "x is larger than zero" << endl;

switch(x) {
case 5 : cout << "x is equal to 5 " << endl;
break;
case 10 : cout << "x is equal to 10" << endl;
break;
default : cout << "x is neither 5 nor 10" << endl;
break;

//Man måste ha en break under varje fråga annars kommer alla "case" skrivas ut.
//Logiskt fel

} // Switchen måste avslutas
  //Syntax-fel
return 0;
} // End main

//-----------------
// Laboration 3, Assignment_1.cpp
// Program som manipulerar strängar
// Marcus Lönnqvist 2018-11-21
//-----------------

#include <iostream>
#include <iomanip>
#include <string>
using namespace std;


int main()
{

 string const inText1 = "Foten är en kroppsdel som förekommer mycket i våra uttryck.";
 string const inText2 = "På stående fot betyder omedelbart, utan förberedelse.";
 string const inText3 = "Försätta på fri fot betyder att ge någon friheten.";
 string const inText4 = "Sätta foten i munnen betyder att göra bort sig.";
 string const inText5 = "Få om bakfoten betyder att missuppfatta något.";
 string const inText6 = "Skrapa med foten betyder att visa sig underdånig eller ödmjuk.";
 string const inText7 = "Stryka på foten betyder att tvingas ge upp.";
 string const inText8 = "Leva på stor fot betyder att föra ett dyrbart eller slösaktigt leverne.";
 string const inText9 = "Varför fick du foten???";

 string allText = inText1 + inText2 + inText3 + inText4 + inText5 + inText6 + inText7 + inText8 + inText9;
 int sizeOfString = allText.size();
 //Lägger ihop alla strings

 cout<<"-----Laboration 3, assignment 1-----"<<endl <<endl;

  while( allText.find("fot") != string::npos )
  {
    allText.replace(allText.find("fot"), 3, "hand");
    //Ersätter fot med hand om första bokstaven är liten
  }

  while( allText.find("Fot") != string::npos)
  {
    allText.replace(allText.find("Fot"), 3, "Hand");
    //Ersätter fot med hand om görsta bokstaven är stor
  }

 int curPos = 1;

  while (curPos != 0)
  {
    curPos = allText.find('.', curPos);
    curPos++;
    allText.insert(curPos, "\n");
  }
    //Lägger en linebreak vid varje punkt

  cout << allText; //Skriver ut den ändrade texten

  return 0;

}

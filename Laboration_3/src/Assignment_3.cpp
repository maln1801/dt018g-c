//-----------------
// Laboration 3, Assignment_3.cpp
// Program som krypterar och dekrypterar text
// Marcus Lönnqvist 2018-01-03
//-----------------

#include <iostream>
#include <iomanip>
#include <string>

using namespace std;


int main()
{
  string input;
  string encrypted;
  string decrypted;
  int p = 0; //Räknare

  cout<<"-----Laboration 3, assignment 3-----"<<endl <<endl;

  cout << "Write anything: ";
  getline(cin, input);

  cout << endl << endl;
  encrypted = input;

  for(size_t i = 0; i < encrypted.length(); i++)
  {
    if(p <= 4)
    {
      encrypted[i] += 13; //Flyttar tecken 13 steg
    }
    if(p > 4)
    {
      encrypted[i] += 7; //Flyttar tecken 7 steg
    }
    //Byter rot efter 5 tecken
    p++;
    if(p == 10)
    {
      p = 0; //Återsäller räknaren
    }

  }

  cout << endl << endl << "Encrypted text: " << encrypted;
  //Skriver ut krypterad text
  cout << endl << endl;
  decrypted = encrypted;

  p = 0; //Återsäller räknaren
  for(size_t i = 0; i < encrypted.length(); i++)
  {
    if(p <= 4)
    {
      decrypted[i] -= 13; //Flyttar tecken 13 steg
    }
    if(p > 4)
    {
      decrypted[i] -= 7; //Flyttar tecken 7 steg
    }
    //Byter rot efter 5 tecken
    p++;
    if(p == 10)
    {
      p = 0; //Återsäller räknaren
    }

  }
  cout << endl << endl << "Decrypted text: " << decrypted;
  //Skriver ut dekryptad text



  return 0;
}

//-----------------
// Laboration 3, Assignment_2.cpp
// Program som sorterar namn
// Marcus Lönnqvist 2018-11-23
//-----------------

#include <iostream>
#include <iomanip>
#include <string>
#include <algorithm>

using namespace std;


int main()
{
  string input0, input1, input2, output, str0, str1, str2; // Användarens inmatning
  int pos; //Position för mellanslag

  cout<<"-----Laboration 3, assignment 2-----"<<endl <<endl;

  cout << "[1/3] Enter a first name and a last name: ";
  getline(cin,input0);
  cout << "[2/3] Enter a first name and a last name: ";
  getline(cin,input1);
  cout << "[3/3] Enter a first name and a last name: ";
  getline(cin,input2);


  pos = input0.find(" ");
  str0 = input0.substr(pos + 1)[0];

  pos = input1.find(" ");
  str1 = input1.substr(pos + 1)[0];

  pos = input2.find(" ");
  str2 = input2.substr(pos + 1)[0];

  //Ovan hittar programmet mellanslag och sparar bokstaven efter i en variabel

  str0 += input0;
  str1 += input1;
  str2 += input2;

  //Programmet lägger på namnet på första bokstaven för att täcka om efternamnen skulle vara lika

  transform(str0.begin(), str0.end(), str0.begin(), ::toupper);
  transform(str1.begin(), str1.end(), str1.begin(), ::toupper);
  transform(str2.begin(), str2.end(), str2.begin(), ::toupper);
  //Gör om alla tecken till stora bokstäver


  cout << endl << endl << "-------------Sorted list-------------" << endl << endl;

  if(str0 < str1 && str1 < str2)
  {
    output = input0 + "\n" + input1 + "\n" + input2;
  }
  else if(str0 < str2 && str2 < str1)
  {
    output = input0 + "\n" + input2 + "\n" + input1;
  }
  else if(str1 < str0 && str0 < str2)
  {
    output = input1 + "\n" + input0 + "\n" + input2;
  }
  else if(str1 < str2 && str2 < str0)
  {
    output = input1 + "\n" + input2 + "\n" + input0;
  }
  else if(str2 < str1 && str1 < str0)
  {
    output = input2 + "\n" + input1 + "\n" + input0;
  }
  else if(str2 < str0 && str0 < str1)
  {
    output = input2 + "\n" + input0 + "\n" + input1;
  }
  else if(str0 == str1)
  {
    output = input2 + "\n" + input0 + "\n" + input1;
  }

  //Ovan går programmet igenom alla villkor och skriver sedan ut det som passar

  cout << output;

  return 0;
}

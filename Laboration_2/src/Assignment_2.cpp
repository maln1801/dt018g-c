//-----------------
// Laboration 2, Assignment_2.cpp
// Program som skriver ut ASCII-tecken
// Marcus Lönnqvist 2018-11-13
//-----------------

#include <iostream>
#include <iomanip>
using namespace std;


int main()
{
  int i = 32;
  //Deklarerar variabeln som jag ska skriva ut sen

  for (int row=1; row <= 32; row++)
  {

    for(int col=1; col <= 7; col++)
    {

      cout << setw(5) << i << " " << (char)i;
      i += 32;
      //Lägger på 32 så att utskriften blir kolumnvis

    }

    cout << endl;


      //Byter rad
    i -= 223; // Går tillbaka till ursprung

    cout << endl;
    //Byter rad
  }
  return 0;

}

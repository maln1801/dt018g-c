//-----------------
// Laboration 2, Assignment_1.cpp
// Program som kollar trianglar
// Marcus Lönnqvist 2018-11-13
//-----------------

#include <iostream>
#include <iomanip>
using namespace std;


int main()
{

  int a, b, c;
  int triangel = 0;
  const int top = 500;
  //Lägger en ribba på 500

  cout<<"-----Phytagoras in the works-----"<<endl <<endl;

  for(a = 1; a<= top; a++){
    for(b = a; b <= top; b++){
      //Startar b där a slutar så jag inte får dubletter
      for(c = 1; c <= top; c++){
        if(c*c == a*a + b*b){
          //Stämmer algoritmen lägger den på 1 på triangel.
          //Om C kan delas med 100 utan rest skriver jag ut den rektangeln
          triangel++;
          if(c%100 == 0){

           cout << endl << "A: " << a << " B: " << b << " C: " << c;
          }
        }
      }
    }
  }

  cout << endl << endl << "Amount of triangles the algoritm worked on: " << triangel;

  return 0;

}

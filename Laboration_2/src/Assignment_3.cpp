//-----------------
// Laboration 2, Assignment_3.cpp
// Program som kollar in inmatat tal är ett palindrom
// Marcus Lönnqvist 2018-11-20
//-----------------

#include <iostream>
#include <iomanip>
using namespace std;


int main()
{
  int input; //Användarens inmatning
  int t; //Plats att lägga inmatning
  int p; // Räknare för hur många siffror i inmantning
  int svar = 1; //Bool för att köra om programmet
  char in; // Inmatning till "svar"


  do
  {
    cout << "-----Is the number a palindrome?-----" << endl << endl;


    while(p != 5)
    {
      p = 0;
      input = 0;
      //Återställer båda variablerna

      cout<<"Enter a 5-digit number: ";
      cin>>input;

      if(cin.fail())
      {
        cin.clear();
        cin.ignore();
      }
      //Hanterar en cin fail ifall input skulle vara en bokstav

      t=input;
      //Lägger inmatningen i en annan variabel innan verifieringen nedan

      while(input)
      {
        input /= 10;
        p++;
      }
      //Kollar att det är 5 siffror

      if(p != 5 ){
        cout << endl << "invalid input, try again" << endl;
      }
      //Ger ett felmeddelande om inmatningen inte är 5 siffror
    }
    //REPEAT USER INPUT IF NOT 5 DIGITS


    if(p == 5)
    {
      int r; // Rest
      int c = t; // Lägger inmatat tal i en annan variabel så vi kan ändra "t"
      int rN = 0; //Det inverterade talet

      while(t != 0)
      {
        r = t % 10;
        rN = (rN * 10) + r;
        t /= 10;
      }

      cout << "Reversed Number = " << rN;

      if(c == rN)
      {

        cout << endl << "The number is a palindrome" << endl;

      }
      else
      {

        cout << endl << "The number is not a palindrome" << endl;

      }

    }

    cout<<endl<<"Would you like to run the program again? y/n ";
    cin >> in;

    if(in == 'y' || in == 'Y')
    {
      svar = 1;
      p=0;
    }
    else
    {
      svar = 0;
    }
    //Kollar om användaren vill köra programmet igen

  }while(svar==1);



  return 0;

}

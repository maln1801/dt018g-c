//-----------------
// main.cpp
// Program som hanterar en lista
// Marcus Lönnqvist 2018-12-15
//-----------------

#include <iostream>
#include <vector>
#include "../include/constants.h"
#include "../include/functions.h"

using namespace std;

int main()
{
  int choice;
  bool again = true;
  vector<Person> persons;
  vector<string> menuVector
  {
    "=========== Menu =============",
    "1. Add a name to the list",
    "2. Print list on screen",
    "3. Search in list",
    "4. Remove element",
    "5. Sort...",
    "6. Write list to a .txt",
    "7. Read from .txt",
    "8. Quit",
    "=============================="
  };


  do
  {
    system("cls");
    cout << endl << "Amount of people in the list: " << persons.size() << endl;
    choice = menu(menuVector);
    switch(choice)
    {
      case 1 : addName(persons);
      break;
      case 2 : printVector(persons);
      break;
      case 3 : findElement(persons);
      break;
      case 4 : removeElement(persons);
      break;
      case 5 : sortList(persons);
      break;
      case 6 : writeToFile(persons);
      break;
      case 7 : readFromFile(persons);
      break;
      case 8 : again = false;
    }

  } while(again);


  return 0;

}

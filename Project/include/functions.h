#ifndef FUNCTIONS_H
#define FUNCTIONS_H
#include "constants.h"
#include <iostream>
#include <vector>


int menu(vector<string> v);
void addName(vector<Person> &v);
void printVector(vector<Person> &v);
void writeToFile(vector<Person> &v);
void readFromFile(vector<Person> &v);
void removeElement(vector<Person> &v);
void sortList(vector<Person> &v);
void findElement(vector<Person> &v);
string generateSignature(string firstNameIn, string lastNameIn, vector<Person> &v);
bool compareName(Person a, Person b);
bool compareHeight(Person &a, Person &b);
bool compareSignature(Person &a, Person &b);

#endif

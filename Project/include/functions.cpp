//Functions.cpp

#include "functions.h"
#include <iostream>     //cout och cin
#include <vector>       //vektorer
#include <string>       //Strängar
#include <iomanip>      //setw, right och left
#include <fstream>      //Filhantering
#include <sstream>      //istringstream
#include <algorithm>    //sort()
#include <random>       //Random engine

using namespace std;


int menu(vector<string> v)
{

  int input;

  for(size_t i = 0; i < v.size(); i++)
  {
    cout << v[i] << endl << endl;
  }
  //Skriver ut menyvalen

  cout << endl << endl << "Enter a number from the list above: ";

  cin >> input;

  while (cin.fail() || input > 8 || input < 1) //Kollar om cin misslyckas eller inte matchar menyn
  {
    cin.clear();
    cin.ignore(100, '\n'); // rensar hela cin vad än användaren skrivit in
    cout << endl << "You've entered an invalid value, try again: ";
    cin >> input;
  }
  //Felkontroll

  return input;

}
void addName(vector<Person> &v)
{
  string firstName;
  string lastName;
  string signature;
  double height;

  system("cls");
  cin.ignore();

  cout << "First name: ";
  getline(cin, firstName);

  cout << "Last name: ";
  getline(cin, lastName);

  cout << "Height [m.cm]: ";
  cin >> height;

  cout << endl << endl << "Adding " << firstName << " " << lastName <<  " to the list";

  signature = generateSignature(firstName, lastName, v); //Genererar signaturen i egen funktion
  Person temp = {signature, firstName, lastName, height};
  v.push_back(temp);

  cout << endl << endl;

  system("pause"); //väntar på användarens inmatning
}
void printVector(vector<Person> &v)
{
  int robinCodeMaster = 0;
  int maxName = 0;

  system("cls");

  for(size_t i = 0; i < v.size(); i++)
  {
    if(maxName < sizeof(v[i].firstName + v[i].lastName))
    {
      maxName = sizeof(v[i].firstName + v[i].lastName);
    }
  }
  //Kollar vilket namn som är störst så att utskriften kan anpassa sig
  cout<< fixed;
  cout<< setprecision(2);
  cout << "-------- Name List --------" << endl;
  cout << "Number of persons in list: " << v.size() << endl << endl;
  cout << left << setw(5) << "Nr" << setw(12) <<  "Sign" << setw(maxName + 2) << "Name" << setw(10) << "Height [m]" << endl;

  //Kollar största namnet

  for (size_t i = 0; i < v.size(); i++)
  {
    robinCodeMaster++;
    cout << left << setw(5) << (i+1) << setw(12) << v[i].signature << setw(maxName + 2) << (v[i].firstName + " " + v[i].lastName) << setw(5) << right << v[i].height << endl;
    if(robinCodeMaster == 20)
    {
      system("pause"); //väntar på användarens inmatning
      system("cls");
      robinCodeMaster =0;
      cout << "-------- Name List --------" << endl;
      cout << "Number of persons in list: " << v.size() << endl << endl;
      cout << left << setw(5) << "Nr" << setw(12) <<  "Sign" << setw(20) << "Name" << setw(10) << "Height [m]" << endl;
    }
  }

  system("pause"); //väntar på användarens inmatning

}
void writeToFile(vector<Person> &v)
{

  string fileName;
  string encryptedString;
  stringstream stream; //För att konvertera flyttal till sträng
  int key;
  cout << "What should the file be called?: ";
  cin >> fileName;
  cout << endl << "Enter enctyption key [no enctyption = 0]: ";
  cin >> key;
  while (cin.fail()) //Kollar om cin misslyckas eller inte matchar menyn
  {
    cin.clear();
    cin.ignore(100, '\n'); // rensar hela cin vad än användaren skrivit in
    cout << endl << "You've entered an invalid value, try again: ";
    cin >> key;
  }
  ofstream outFile(fileName + ".txt"); //Bestämmer filnamn

  for(size_t i = 0; i < v.size(); i++)
  {
    stream.str(""); //Rensar stringstram
    stream << fixed << setprecision(2) << v[i].height; //Sätter precision för flyttalet
    string height = stream.str(); //Sparar streamen som en vanlig string
    encryptedString += (v[i].signature + DELIM + v[i].firstName + DELIM + v[i].lastName + DELIM + height + '\n');
    // Lägger in person i variabel, rad för rad
  }
  for(size_t i = 0; i < encryptedString.size(); i++)
  {
    if(encryptedString[i] != '\n')
    {
      encryptedString[i] += key;
    }
    //Ser till att krypteringen inte rör radbrytningen
  }
  //Krypterar strängen som skrivs ut till fil

  outFile << encryptedString;
  //Skriver sist ut strängen till filen

  outFile.close();
  cout << endl << endl << "List written to file successfully" << endl;
  system("pause"); //Väntar på användarens inmatning

}
void readFromFile(vector<Person> &v)
{
  string fileName;
  Person toBeAdded;
  string dectryptedString;
  int amount;
  int key;


  cout << "What file do you want to read from?: ";
  cin >> fileName;
  cout << endl << "Decryption key [no enctyption = 0]: ";
  cin >> key;
  while (cin.fail()) //Kollar om cin misslyckas eller inte matchar menyn
  {
    cin.clear();
    cin.ignore(100, '\n'); // rensar hela cin vad än användaren skrivit in
    cout << endl << "You've entered an invalid value, try again: ";
    cin >> key;
  }

  ifstream inFile((fileName + ".txt")); //Lägger på .txt så att användaren endast behöver skriva in filnamnet

  if(inFile.is_open())
  {

    v.clear(); //Rensar vectorn så att den överskrivs
    amount = 0; //Nollställer räknaren

    while(getline(inFile, dectryptedString))
    {
      for(size_t i = 0; i < dectryptedString.size(); i++)
      {
        dectryptedString[i] -= key;
      }
      //Flyttar alla tecken i strängen med förbestämt

      istringstream iss(dectryptedString);

      while(getline(iss, toBeAdded.signature, DELIM))
      {
        getline(iss, toBeAdded.firstName, DELIM);
        getline(iss, toBeAdded.lastName, DELIM);
        iss >> toBeAdded.height;
        iss.get();
        v.push_back(toBeAdded);
        amount++;
        //Lägger in varje del innan delim i respektive variabel

        iss.clear(); //Rensar istringstram
      }
      // Läser varje del av raden innan delim och läggger den i toBeAdded för att sen kunna trycka in den i den riktiga vectorn
    }

    cout << endl << "Copied " << amount  << " lines" << endl << endl;
    inFile.close();

  }
  else
  {
    cout << "Unable to open file" << endl;
  }

  system("pause"); //Väntar på användarens inmatning

}
void removeElement(vector<Person> &v)
{
  bool found = false;
  string input;

  system("cls");
  cout << "-------- Name List --------" << endl;
  cout << "Number of persons in list: " << v.size() << endl << endl;
  cout << left << setw(5) << "Nr" << setw(12) <<  "Sign" << setw(20) << "Name" << setw(10) << "Lenght [m]" << endl;

  for(size_t i = 0; i < v.size(); i++)
  {
    cout << left << setw(5) << (i+1) << setw(12) << v[i].signature << setw(20) << (v[i].firstName + " " + v[i].lastName) << setw(5) << right << v[i].height << endl;
  }
  cout << endl << endl;
  //Skriver ut listan för att kunna se vad man ska ta bort

  cout << "Enter signature of element to be removed: ";
  cin.ignore(); //Rensar cin för att kunna köra getline()
  getline(cin, input);

  for(size_t i = 0; i < v.size(); i++)
  {
    if(input == v[i].signature)
    {
      cout << endl << v[i].firstName << " removed" << endl << endl;
      v.erase (v.begin()+i);
      found = true;
    }
    //Om signaturen hittas tas den platsen bort
  }
  if(!found)
  {
    cout << endl << endl << "Could not find " << input << endl << endl;
  }

  system("pause"); //Väntar på användarens inmatning

}
void findElement(vector<Person> &v)
{
  system("cls");

  string input;
  bool found = false;

  cout << "Enter signature to search for: ";
  cin.ignore(); //Rensar cin för att kunna köra getline() utan fel
  getline(cin,input);

  for(size_t i = 0; i < v.size(); i++)
  {
    if(input == v[i].signature)
    {
      cout << "Result:" << endl << endl;
      cout << left << setw(12) <<  "Sign" << setw(20) << "Name" << setw(10) << "Lenght [m]" << endl << endl;
      cout << left << setw(12) << v[i].signature << setw(20) << (v[i].firstName + " " + v[i].lastName) << setw(5) << right << v[i].height << endl << endl;

      found = true;
    }
    //Om signaturen hittas skrivs hela elementet ut
  }
  if(!found)
  {
    cout << endl << endl << "Could not find " << input << endl << endl;
  }

  system("pause");
}
void sortList(vector<Person> &v)
{
  auto randomEngine = default_random_engine {};
  int input;

  cin.ignore();
  cout << endl << "Sort choosed, please choose wich way to sort" << endl;
  cout << endl << "1. by last name" << endl;
  cout << endl << "2. by signature " << endl;
  cout << endl << "3. by height " << endl;
  cout << endl << "4. Shuffle the list! " << endl;
  cout << endl << "Enter a number: ";
  cin >> input;

  switch(input)
  {
    case sortName : sort(v.begin(), v.end(), compareName);
    cout  << endl << "The list is now sorted"  << endl;
    system("pause");
    break;

    case sortSign : sort(v.begin(), v.end(), compareSignature);
    cout  << endl << "The list is now sorted by signature"  << endl;
    system("pause");
    break;

    case sortHeight : sort(v.begin(), v.end(), compareHeight);
    cout  << endl << "The list is now sorted by height"  << endl;
    system("pause");
    break;

    case sortShuffle : shuffle(v.begin(), v.end(), randomEngine);
    cout  << endl << "The list is now shuffled"  << endl;
    system("pause");
  }

 //Sorterar på efternamn i bokstavsordning med egen funktion som jämför efternamn
}
string generateSignature(string firstNameIn, string lastNameIn, vector<Person> &v)
{
  int serialNumber = 1;
  string shortFirst;
  string shortLast;
  string signature;

  while(shortFirst.size() < 3)
  {
    string tempName = firstNameIn.substr(0, 3);

    if(firstNameIn.size() < 3)
    {
      tempName += "x";
    }

    shortFirst = tempName;

  }
// Hittar de tre första bokstäverna i namnet och lägger dem i en variabel
// Lägger till x efter namnet om det är kortare än 3 bokstäver
  while(shortLast.size() < 3)
  {
    string tempName = lastNameIn.substr(0, 3);

    if(lastNameIn.size() < 3)
    {
      tempName += "x";
    }

    shortLast = tempName;

  }
// Hittar de tre första bokstäverna i namnet och lägger dem i en variabel
// Lägger till y efter namnet om det är kortare än 3 bokstäver

  signature = shortFirst + shortLast;
  //Lägger på de tre förnamnsbokstäverna samt efternamnsbokstäverna
  //För att sedan kunna jämföra dem med andra signaturer i listan

  for (size_t i = 0; i < v.size(); i++)
  {
    if(signature == v[i].signature.substr(0, 6))
    {
      serialNumber++;
    }
  }
//Kollar signaturen blir samma som någon annan i vektorn
//Om det gör det så blir serialNumber större

  signature += ("0" + to_string(serialNumber));
  return signature;

}
bool compareName(Person a, Person b)
{
  transform(a.lastName.begin(), a.lastName.end(), a.lastName.begin(), ::toupper);
  transform(b.lastName.begin(), b.lastName.end(), b.lastName.begin(), ::toupper);
  if(a.lastName == b.lastName)
  {
    transform(a.firstName.begin(), a.firstName.end(), a.firstName.begin(), ::toupper);
    transform(b.firstName.begin(), b.firstName.end(), b.firstName.begin(), ::toupper);
    return a.firstName < b.firstName;
  }
  else
  {
    return a.lastName < b.lastName;
  }
  //Ger denna funktion ett värde som funktionen sortList() kan använda
  //Funktionen gör tillfälligt alla namn med stora bokstäver så att den kan sortera även små bokstäver.
}
bool compareSignature(Person &a, Person &b)
{
  return a.signature < b.signature;
  //Ger denna funktion ett värde som funktionen sortList() kan använda
}
bool compareHeight(Person &a, Person &b)
{
  return a.height > b.height;
  //Ger denna funktion ett värde som funktionen sortList() kan använda
}

#ifndef CONSTANTS_H
#define CONSTANTS_H
#include <iostream>
#include <string>
#include <vector>

using namespace std;

const char DELIM = '|';
struct Person
{
  string signature;
  string firstName;
  string lastName;
  double height;
};
enum SortAlt
{
  sortName = 1, sortSign = 2, sortHeight = 3, sortShuffle = 4
};


#endif

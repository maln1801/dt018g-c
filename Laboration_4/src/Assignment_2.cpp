//-----------------
// Laboration 4, Assignment_2.cpp
// Program som räknar ut kostnad för samtal
// Marcus Lönnqvist 2019-01-18
//-----------------

#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <ctime> //time();

using namespace std;
int toMinutes(string call);
bool validTime(string callStartIn, string callEndIn);
int samtalsKostnad(int callStart, int callEnd);
float applyExtra(float price, int callStart, int callEnd);
int again();


int main()
{
  string callStartIn, callEndIn;
  int callStart, callEnd;
  float price;

  cout << "-----Laboration 4 Assignment 2-----" << endl << endl;
  cout << "RingPling2004 AB" << endl;
  cout << "________________" << endl << endl;
  do
  {

    do
    {

      cout << "When did the call begin? [hh:mm or h:mm]: ";
      getline(cin, callStartIn);
      cout << "When did it end? [hh:mm or h:mm]: ";
      getline(cin, callEndIn);

    }while(!validTime(callStartIn, callEndIn));

    callStart = toMinutes(callStartIn);
    callEnd = toMinutes(callEndIn);

    price = samtalsKostnad(callStart, callEnd);
    price = applyExtra(price, callStart, callEnd);

    cout << endl << "Total cost: " << price << "kr";

  }while (again() == 1);

  return 0;

}

bool validTime(string callStartIn,string callEndIn)
{
  int pos, callStartH, callStartM, callEndH, callEndM;
  string cs = callStartIn, ce = callEndIn;
  //Gör extravariabler som jag kan sätta in i istringstream för att jämföra
  pos = cs.find(':');
  cs[pos] = ' ';
  //Ersätter ":" med ett mellanslag så att de kan separeras nedan
  istringstream iss(cs);
  iss >> callStartH >> callStartM;
  pos = ce.find(':');
  ce[pos] = ' ';
  //Ersätter ":" med ett mellanslag så att de kan separeras nedan
  iss.clear();
  iss.str(ce);
  iss >> callEndH >> callEndM;

  if(toMinutes(callStartIn) < 1440 && toMinutes(callEndIn) < 1440 && toMinutes(callStartIn) < toMinutes(callEndIn) && callStartM < 60 && callEndM < 60)
  {
    return true;
  }
  //if kollar att inmatningen inte överstiger 23:59 och om start är större än slut
  else
  {
    cout << endl << "Input a valid time" << endl << endl;
    return false;
  }

}

int toMinutes(string call)
{
  int pos, callH, callM;

  pos = call.find(':');
  call[pos] = ' ';
  //Ersätter ":" med ett mellanslag så att de kan separeras nedan
  istringstream iss(call);
  iss >> callH >> callM;
  //Sparar timmar och minuter i separata variabler

  callM += callH * 60;
  //Gör om allt till minuter

  return callM; //Returnerar minuter

}

int samtalsKostnad(int callStart, int callEnd)
{
  const int pricePerMinute = 4; //4kr per minut

  return (callEnd - callStart) * pricePerMinute;
}

float applyExtra(float price, int callStart, int callEnd)
{
  const float bigSale = 0.65; // 65%
  const float smallSale = 0.15; // 15%
  const float tax = 1.25; // 25%
  const int night = 1110; // 18:30 i minuter
  const int morning = 480; // 8:00 i minuter



//---------------------------------------------------------------------------------------------------------------
//  Kväll
//---------------------------------------------------------------------------------------------------------------
  if(callStart >= night && callEnd > night)
  {
    price -= price * bigSale; //Tar bort 35% från standardtaxan

    cout << "Discount on : " << callEnd -callStart << " min " << endl; //<-------Debug
  }
  else if (callStart < night && callEnd >= night)
  {
    price -= (callEnd - night)* 4 * bigSale; //Tar bort 35% från standardtaxan efter 18:00

    cout << "Discount on : " << callEnd - night << " min " << endl; //<-------14:Debug
  }
//---------------------------------------------------------------------------------------------------------------
//  Morgon
//---------------------------------------------------------------------------------------------------------------
  if(callStart < morning && callEnd < morning)
  {
    price -= price * bigSale;  //Tar bort 35% från standardtaxan

    cout << "Discount on : " << callEnd -callStart << " min " << endl; //<-------//Debug
  }
  else if (callStart < morning && callEnd > morning)
  {
    price -= (morning - callStart)* 4 * bigSale;  //Tar bort 35% från standardtaxan innan 8:00

    cout << "Discount on : " << morning - callStart << " min " << endl; //<-------Debug

  }

  if(callEnd-callStart > 30)
  {
    price -= price*smallSale; //Tar bort 15% från standardtaxan
  }

  price *= tax; //Lägger på moms

  return price;


}

int again()
{
  string in;
  cout << endl << endl << "Do you want to make another calculation? [y / any key]: ";
  getline(cin, in);
  if(in == "y" || in == "Y")
  {
    return 1;

  }
  else
  {
    return 0;
  }

}

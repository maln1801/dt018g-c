//-----------------
// Laboration 4, Assignment_1.cpp
// Program som simulerar tärningar
// Marcus Lönnqvist 2019-01-18
//-----------------

#include <iostream>
#include <iomanip>
#include <string>
#include <ctime> //time();
#include <random>

using namespace std;

void throwDice();

int main()
{

  char input = 'y';

  do
  {
    system("cls");
    cout << "-----Laboration 4 Assignment 1-----" << endl << endl;
    throwDice();
    cout  << endl << "Do you want to throw the dice again? [y/n]:";
    cin >> input;
  }while(input == 'y' || input == 'Y');


  return 0;
}

void throwDice()
{
  random_device rd;
  mt19937 mt(rd());
  uniform_int_distribution<int> distribution(1, 6);

  //Varje gång programmet kör får denna funktionen ett unikt värde
  //På så sätt blir rand() unikt vid varje körning.

  int throws, one=0, two=0, three=0, four=0, five=0, six=0;
  double amount;

  cout << "How many times do you wanna throw the dice?: ";
  cin >> throws;
  for (int i = 0; i < throws; i++)
  {
    //amount = rand() % 6 + 1;
  //  amount = distribution(generator);
    switch(distribution(mt))
    {
      case 1: one++;
      break;
      case 2: two++;
      break;
      case 3: three++;
      break;
      case 4: four++;
      break;
      case 5: five++;
      break;
      case 6: six++;
    }
  }

  //Slumpar och skriver i hur många det blev av varje

  cout.precision(2);
  cout << fixed;

  //Skriver ut svaret med 2 decimaler
  cout << endl << endl << "amount of ones: " << one << "  |  " <<(double)one/throws*100 << "%" << endl;
  cout << endl << endl << "amount of twos: " << two << "  |  " << (double)two/throws*100 << "%" << endl;
  cout << endl << endl << "amount of threes: " << three << "  |  " << (double)three/throws*100 << "%" << endl;
  cout << endl << endl << "amount of fours: " << four << "  |  " << (double)four/throws*100 << "%" << endl;
  cout << endl << endl << "amount of fives: " << five << "  |  " << (double)five/throws*100 << "%" << endl;
  cout << endl << endl << "amount of sixes: " << six << "  |  " << (double)six/throws*100 << "%" << endl;
  //Skriver ut procent
}
